% =============================================================================
% dissertation.cls
% Document class for disserations based on dbis-thesis.cls from DBIS-Lehrstuhl 
% at FSU Jena by Matthias Liebisch (26/05/2011).
%
% 
% Author:             Alexandra Diem  
% =============================================================================
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{dissertation}[2011/05/12 v1.0 dbis-thesis]
\typeout{Document class for disserations}
\LoadClass[12pt,a4paper,twoside]{book}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{ifthen}

% ----------------------------------------------------------------------------
% Margin, Spacing, Counters
% ----------------------------------------------------------------------------
\setlength{\topmargin}{0.0cm}
\setlength{\textheight}{23.0cm}
\setlength{\textwidth}{15.0cm}
\setlength{\headsep}{0.8cm}
\setlength{\oddsidemargin}{0.9cm}
\addtolength{\evensidemargin}{-2.49cm}
\setlength{\parindent}{0pt}
\setlength{\parskip}{5pt plus 2pt minus 1pt}
\setcounter{secnumdepth}{3}
\setcounter{tocdepth}{3}
\setcounter{topnumber}{9}
\setcounter{totalnumber}{9}
\renewcommand{\topfraction}{0.99}
\renewcommand{\textfraction}{0.01}

% abbreviations
\newcommand{\eg}{e.\,g.\ }
\newcommand{\cf}{c.\,f.\ }
\newcommand{\etc}{etc.\ }
\newcommand{\ie}{i.\,e.\ }

% no widows or orphans
\clubpenalty = 10000
\widowpenalty = 10000
\displaywidowpenalty = 10000

% ----------------------------------------------------------------------------
% Interface Parameters
% ----------------------------------------------------------------------------
% inititalise internal makros
\newcommand{\@thesisTitle}{}
\newcommand{\@thesisAuthor}{}
\newcommand{\@thesisDegree}{}
\newcommand{\@thesisTutor}{}
\newcommand{\@thesisDate}{}
\newcommand{\@thesisUniversity}{}
% set makros
\newcommand{\thesisTitle}[1]{\renewcommand{\@thesisTitle}{#1}}
\newcommand{\thesisAuthor}[1]{\renewcommand{\@thesisAuthor}{#1}}
\newcommand{\thesisDegree}[1]{\renewcommand{\@thesisDegree}{#1}}
\newcommand{\thesisTutor}[1]{\renewcommand{\@thesisTutor}{#1}}
\newcommand{\thesisDate}[1]{\renewcommand{\@thesisDate}{#1}}
\newcommand{\thesisUniversity}[1]{\renewcommand{\@thesisUniversity}{#1}}

% ----------------------------------------------------------------------------
% Titlepage
% ----------------------------------------------------------------------------
\newcommand{\thesisMakeTitle}[0]{
  \newpage
  \thispagestyle{empty}
  \null
  \begin{center}
    \large{\@thesisUniversity} \\ [1ex]
    \vfill
      {\huge\textbf{\@thesisTitle}\par}
    \vfill
      { by} \\ [1ex]
      {\Large \@thesisAuthor} \\ [1ex]
      \@thesisDate
    \vfill
      \ifthenelse{\equal{\@thesisDegree}{}}{}{A dissertation submitted in partial fulfillment of the degree of}
    \vskip 1ex
      {\Large \@thesisDegree}
    \vskip 1ex
    \ifthenelse{\equal{\@thesisDegree}{}}{}{by examination and dissertation.}
  \end{center}
}