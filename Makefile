# LaTeX Makefile

FILE=dissertation

all: $(FILE).pdf

clean:
	\rm *.aux *.blg *.out *.bbl *.log *.toc

$(FILE).pdf: $(FILE).tex
	pdflatex $(FILE)
	pdflatex $(FILE)
	bibtex $(FILE)
	pdflatex $(FILE)
	pdflatex $(FILE)

